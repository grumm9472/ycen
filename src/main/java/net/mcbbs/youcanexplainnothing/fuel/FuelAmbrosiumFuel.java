
package net.mcbbs.youcanexplainnothing.fuel;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemAmbrosium;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class FuelAmbrosiumFuel extends ElementsYouCanExplainNothingMod.ModElement {
	public FuelAmbrosiumFuel(ElementsYouCanExplainNothingMod instance) {
		super(instance, 475);
	}

	@Override
	public int addFuel(ItemStack fuel) {
		if (fuel.getItem() == new ItemStack(ItemAmbrosium.block, (int) (1)).getItem())
			return 1600;
		return 0;
	}
}
