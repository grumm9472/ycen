
package net.mcbbs.youcanexplainnothing.fuel;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemBlastCoal;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class FuelBlastCoalFuel extends ElementsYouCanExplainNothingMod.ModElement {
	public FuelBlastCoalFuel(ElementsYouCanExplainNothingMod instance) {
		super(instance, 474);
	}

	@Override
	public int addFuel(ItemStack fuel) {
		if (fuel.getItem() == new ItemStack(ItemBlastCoal.block, (int) (1)).getItem())
			return 9600;
		return 0;
	}
}
