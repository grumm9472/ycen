
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.block.BlockStoneOfStars;
import net.mcbbs.youcanexplainnothing.block.BlockCrackedStoneOfStars;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeCrackedStoneOfStarsSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeCrackedStoneOfStarsSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 464);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockStoneOfStars.block, (int) (1)), new ItemStack(BlockCrackedStoneOfStars.block, (int) (1)), 1F);
	}
}
