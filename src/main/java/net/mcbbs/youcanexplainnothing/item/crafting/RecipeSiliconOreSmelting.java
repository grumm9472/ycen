
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemSilicon;
import net.mcbbs.youcanexplainnothing.block.BlockSiliconOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeSiliconOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeSiliconOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 319);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockSiliconOre.block, (int) (1)), new ItemStack(ItemSilicon.block, (int) (1)), 0.7F);
	}
}
