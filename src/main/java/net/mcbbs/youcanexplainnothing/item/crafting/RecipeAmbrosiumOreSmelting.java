
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemAmbrosium;
import net.mcbbs.youcanexplainnothing.block.BlockAmbrosiumOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeAmbrosiumOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeAmbrosiumOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 471);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockAmbrosiumOre.block, (int) (1)), new ItemStack(ItemAmbrosium.block, (int) (1)), 0.7F);
	}
}
