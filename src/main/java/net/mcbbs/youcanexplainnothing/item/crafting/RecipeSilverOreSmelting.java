
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemSilverIngot;
import net.mcbbs.youcanexplainnothing.block.BlockSilverOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeSilverOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeSilverOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 553);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockSilverOre.block, (int) (1)), new ItemStack(ItemSilverIngot.block, (int) (1)), 0.7F);
	}
}
