
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.block.BlockStoneOfStars;
import net.mcbbs.youcanexplainnothing.block.BlockCobblestoneOfStars;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeStoneOfStarsSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeStoneOfStarsSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 453);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockCobblestoneOfStars.block, (int) (1)), new ItemStack(BlockStoneOfStars.block, (int) (1)), 1F);
	}
}
