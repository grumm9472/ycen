
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemGermaniumIngot;
import net.mcbbs.youcanexplainnothing.block.BlockGermaniumOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeGermaniumOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeGermaniumOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 529);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockGermaniumOre.block, (int) (1)), new ItemStack(ItemGermaniumIngot.block, (int) (1)), 0.7F);
	}
}
