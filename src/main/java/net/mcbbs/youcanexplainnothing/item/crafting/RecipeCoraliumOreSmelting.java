
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemCoraliumIngot;
import net.mcbbs.youcanexplainnothing.block.BlockCoraliumOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeCoraliumOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeCoraliumOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 405);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockCoraliumOre.block, (int) (1)), new ItemStack(ItemCoraliumIngot.block, (int) (1)), 0.7F);
	}
}
