
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemLongom;
import net.mcbbs.youcanexplainnothing.block.BlockLongomOre;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeLongomOreSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeLongomOreSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 574);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockLongomOre.block, (int) (1)), new ItemStack(ItemLongom.block, (int) (1)), 0.7F);
	}
}
