
package net.mcbbs.youcanexplainnothing.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcbbs.youcanexplainnothing.item.ItemRendersteelNugget;
import net.mcbbs.youcanexplainnothing.item.ItemRendersteelDust;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class RecipeRendersteelNuggetSmelting extends ElementsYouCanExplainNothingMod.ModElement {
	public RecipeRendersteelNuggetSmelting(ElementsYouCanExplainNothingMod instance) {
		super(instance, 380);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(ItemRendersteelDust.block, (int) (1)), new ItemStack(ItemRendersteelNugget.block, (int) (1)), 5F);
	}
}
