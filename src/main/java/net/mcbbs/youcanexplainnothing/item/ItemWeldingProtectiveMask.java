
package net.mcbbs.youcanexplainnothing.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcbbs.youcanexplainnothing.creativetab.TabNothingSurface;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ItemWeldingProtectiveMask extends ElementsYouCanExplainNothingMod.ModElement {
	@GameRegistry.ObjectHolder("you_can_explain_nothing:welding_protective_maskhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("you_can_explain_nothing:welding_protective_maskbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("you_can_explain_nothing:welding_protective_masklegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("you_can_explain_nothing:welding_protective_maskboots")
	public static final Item boots = null;
	public ItemWeldingProtectiveMask(ElementsYouCanExplainNothingMod instance) {
		super(instance, 312);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("WELDING_PROTECTIVE_MASK", "you_can_explain_nothing:ron", 25,
				new int[]{2, 5, 6, 3}, 19, (net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")),
				0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("welding_protective_maskhelmet")
				.setRegistryName("welding_protective_maskhelmet").setCreativeTab(TabNothingSurface.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0,
				new ModelResourceLocation("you_can_explain_nothing:welding_protective_maskhelmet", "inventory"));
	}
}
