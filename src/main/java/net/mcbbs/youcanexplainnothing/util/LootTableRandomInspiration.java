
package net.mcbbs.youcanexplainnothing.util;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.util.ResourceLocation;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class LootTableRandomInspiration extends ElementsYouCanExplainNothingMod.ModElement {
	public LootTableRandomInspiration(ElementsYouCanExplainNothingMod instance) {
		super(instance, 341);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		LootTableList.register(new ResourceLocation("you_can_explain_nothing", "random_inspiration"));
	}
}
