
package net.mcbbs.youcanexplainnothing.util;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.util.ResourceLocation;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class LootTableIronDustLootTable extends ElementsYouCanExplainNothingMod.ModElement {
	public LootTableIronDustLootTable(ElementsYouCanExplainNothingMod instance) {
		super(instance, 375);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		LootTableList.register(new ResourceLocation("you_can_explain_nothing", "iron_dust_loot_table"));
	}
}
