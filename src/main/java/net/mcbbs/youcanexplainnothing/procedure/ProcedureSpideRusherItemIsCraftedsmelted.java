package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Enchantments;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureSpideRusherItemIsCraftedsmelted extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureSpideRusherItemIsCraftedsmelted(ElementsYouCanExplainNothingMod instance) {
		super(instance, 489);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("itemstack") == null) {
			System.err.println("Failed to load dependency itemstack for procedure SpideRusherItemIsCraftedsmelted!");
			return;
		}
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		((itemstack)).addEnchantment(Enchantments.THORNS, (int) 3000);
		((itemstack)).addEnchantment(Enchantments.KNOCKBACK, (int) 5);
		((itemstack)).addEnchantment(Enchantments.LOOTING, (int) 5);
	}
}
