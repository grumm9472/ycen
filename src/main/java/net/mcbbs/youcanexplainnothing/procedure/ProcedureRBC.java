package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.world.World;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.math.BlockPos;
import net.minecraft.tileentity.TileEntityLockableLoot;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScoreCriteria;
import net.minecraft.scoreboard.Score;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import net.mcbbs.youcanexplainnothing.item.ItemWastedPaper;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Random;
import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureRBC extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureRBC(ElementsYouCanExplainNothingMod instance) {
		super(instance, 338);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure RBC!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure RBC!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure RBC!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure RBC!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure RBC!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if (((new Object() {
			public int getScore(String score) {
				if (entity instanceof EntityPlayer) {
					Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
					ScoreObjective _so = _sc.getObjective(score);
					if (_so != null) {
						Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
						return _scr.getScorePoints();
					}
				}
				return 0;
			}
		}.getScore("unresearchable")) == 0)) {
			if (((new Object() {
				public int getScore(String score) {
					if (entity instanceof EntityPlayer) {
						Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
						ScoreObjective _so = _sc.getObjective(score);
						if (_so != null) {
							Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
							return _scr.getScorePoints();
						}
					}
					return 0;
				}
			}.getScore("inspiration")) >= 100)) {
				if (((new Object() {
					public int getAmount(BlockPos pos, int sltid) {
						TileEntity inv = world.getTileEntity(pos);
						if (inv instanceof TileEntityLockableLoot) {
							ItemStack stack = ((TileEntityLockableLoot) inv).getStackInSlot(sltid);
							if (stack != null)
								return stack.getCount();
						}
						return 0;
					}
				}.getAmount(new BlockPos((int) x, (int) y, (int) z), (int) (0))) >= 1)) {
					if (((new Object() {
						public int getAmount(BlockPos pos, int sltid) {
							TileEntity inv = world.getTileEntity(pos);
							if (inv instanceof TileEntityLockableLoot) {
								ItemStack stack = ((TileEntityLockableLoot) inv).getStackInSlot(sltid);
								if (stack != null)
									return stack.getCount();
							}
							return 0;
						}
					}.getAmount(new BlockPos((int) x, (int) y, (int) z), (int) (1))) >= 1)) {
						if (entity instanceof EntityPlayer) {
							Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
							ScoreObjective _so = _sc.getObjective("inspiration");
							if (_so == null) {
								_so = _sc.addScoreObjective("inspiration", ScoreCriteria.DUMMY);
							}
							Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
							_scr.setScorePoints((int) ((new Object() {
								public int getScore(String score) {
									if (entity instanceof EntityPlayer) {
										Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
										ScoreObjective _so = _sc.getObjective(score);
										if (_so != null) {
											Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
											return _scr.getScorePoints();
										}
									}
									return 0;
								}
							}.getScore("inspiration")) - 100));
						}
						{
							TileEntity inv = world.getTileEntity(new BlockPos((int) x, (int) y, (int) z));
							if (inv != null && (inv instanceof TileEntityLockableLoot)) {
								ItemStack stack = ((TileEntityLockableLoot) inv).getStackInSlot((int) (0));
								if (stack != null) {
									if (stack.attemptDamageItem((int) 1, new Random(), null)) {
										stack.shrink(1);
										stack.setItemDamage(0);
									}
								}
							}
						}
						{
							TileEntity inv = world.getTileEntity(new BlockPos((int) x, (int) y, (int) z));
							if (inv instanceof TileEntityLockableLoot)
								((TileEntityLockableLoot) inv).decrStackSize((int) (1), (int) (1));
						}
						if ((Math.random() < 0.5)) {
							if (entity instanceof EntityPlayer && !entity.world.isRemote) {
								((EntityPlayer) entity).sendStatusMessage(new TextComponentString("Oops! Not a good thought!"), (true));
							}
						} else {
							if (entity instanceof EntityPlayer) {
								ItemStack _setstack = new ItemStack(ItemWastedPaper.block, (int) (1));
								_setstack.setCount(1);
								ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
							}
						}
					}
				}
			}
		}
	}
}
