package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraft.util.DamageSource;
import net.minecraft.entity.Entity;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureBabaFoodEaten extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureBabaFoodEaten(ElementsYouCanExplainNothingMod instance) {
		super(instance, 496);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure BabaFoodEaten!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		entity.attackEntityFrom(DamageSource.GENERIC, (float) 1);
	}
}
