package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.init.Blocks;
import net.minecraft.entity.Entity;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureWaterCommandCommandExecuted extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureWaterCommandCommandExecuted(ElementsYouCanExplainNothingMod instance) {
		super(instance, 491);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure WaterCommandCommandExecuted!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure WaterCommandCommandExecuted!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		World world = (World) dependencies.get("world");
		world.setBlockState(new BlockPos((int) (entity.posX), (int) ((entity.posY) + 2), (int) (entity.posZ)), Blocks.FLOWING_WATER.getDefaultState(),
				3);
	}
}
