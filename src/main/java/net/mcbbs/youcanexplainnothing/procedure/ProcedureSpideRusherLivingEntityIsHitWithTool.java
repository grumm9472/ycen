package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraft.entity.Entity;

import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureSpideRusherLivingEntityIsHitWithTool extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureSpideRusherLivingEntityIsHitWithTool(ElementsYouCanExplainNothingMod instance) {
		super(instance, 488);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure SpideRusherLivingEntityIsHitWithTool!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		entity.setInWeb();
		entity.setFire((int) 1);
	}
}
