package net.mcbbs.youcanexplainnothing.procedure;

import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import net.mcbbs.youcanexplainnothing.item.ItemAsoxItp;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

import java.util.Map;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class ProcedureAsoxItpItemInInventoryTick extends ElementsYouCanExplainNothingMod.ModElement {
	public ProcedureAsoxItpItemInInventoryTick(ElementsYouCanExplainNothingMod instance) {
		super(instance, 546);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure AsoxItpItemInInventoryTick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityPlayer)
			((EntityPlayer) entity).inventory.clearMatchingItems(new ItemStack(ItemAsoxItp.block, (int) (1)).getItem(), -1, (int) 1, null);
	}
}
