
package net.mcbbs.youcanexplainnothing.creativetab;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import net.mcbbs.youcanexplainnothing.block.BlockStoneOfStars;
import net.mcbbs.youcanexplainnothing.ElementsYouCanExplainNothingMod;

@ElementsYouCanExplainNothingMod.ModElement.Tag
public class TabStarhill extends ElementsYouCanExplainNothingMod.ModElement {
	public TabStarhill(ElementsYouCanExplainNothingMod instance) {
		super(instance, 465);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabstarhill") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(BlockStoneOfStars.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
